import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BeerService {

  beers: Object[] = [
      {
          name: 'Vodka',
          price: 23
      },
      {
          name: 'vodka grey goose',
          price: 25
      },
      {
          name: 'Jack daniel',
          price: 35
      },
      {
          name: 'heineken',
          price: 15
      }
  ];
  constructor() { }

  addBeer(beer)
  {
      this.beers.push(beer);
  }

}
