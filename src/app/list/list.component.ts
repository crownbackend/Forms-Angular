import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BeerService } from '../service/beer.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private http: HttpClient, private  beerService: BeerService) { }

  beers: Object[];

  ngOnInit() {
   this.beers = this.beerService.beers;
  }

  getBeers()
  {
      return this.http.get('https://api.punkapi.com/v2/beers');
  }

}
